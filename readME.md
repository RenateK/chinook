About the project
This project is about to register data in database by using PgAdmin4 or Intellij as a tool. Group members are
Renate Karlsen og Günel Shirinova. 
Assignment is divided into two seperate parts: 
Part 1 or Appendix A is about to modify data by using pgAdmin og Intellij can also be used. 


About Appendix A: 
Scripts folder was created for having all script files inside. We first create three tables, and
so write sql scripts to insert, update and/or delete data from our database. PgAdmin was used to create the database.
Then sql scripts were checked in Intellij also, and the scripts run as expected. 

About Appendix B: 
Chinook Database: This database is used to work with customer data and modify it. The script we are given 
to work with, is run and there were created 16 tables. Customer table have the all information about the customer
and that's the table we worked most. Below we describe the methods created to modify the data:

-List<Customer> findAll() - this methods returns a list of all customers from customer table. Information that 
displayed about the customer is: customer_id, first name, lastname, country, postal code, phone and email.
-Customer findById() - it is used to return a specific customer given by id. 
-Customer findByName() - returns a specific customer given by customer first name
-List<Customer> getCustomerByPage() - returns a list of customers from the given id to the number that is given in sql
-int insert(Customer customer) - this simply adds a new customer to the database
-int update(Customer customer) - updates customer first name 
-CustomerCountry findMaxCountry() - this method is used to find the country with most customers
-CustomerSpender findHighestSpender() - finds the highest spender