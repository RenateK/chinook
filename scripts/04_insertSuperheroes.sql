INSERT INTO superhero (hero_name, hero_alias, hero_origin) 
VALUES ('Oda Kaspersen', 'Crazy Hamster', 'Scotland');

INSERT INTO superhero (hero_name, hero_alias, hero_origin) 
VALUES ('Bjarne Bjørn', 'Beer Bear', 'New York 2039');

INSERT INTO superhero (hero_name, hero_alias, hero_origin) 
VALUES ('Jelena Pettersen', 'JeLo Jazz', 'The Moon');
