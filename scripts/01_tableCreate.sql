DROP TABLE IF EXISTS superhero;
CREATE TABLE superhero (
    hero_id serial PRIMARY KEY,
    hero_name varchar(50) NOT NULL,
    hero_alias varchar(50),
    hero_origin varchar(50)
);

DROP TABLE IF EXISTS power;
CREATE TABLE power (
    power_id serial PRIMARY KEY,
    power_name varchar(50) NOT NULL, 
   description varchar(200)
   
);

DROP TABLE IF EXISTS assistant;
CREATE TABLE assistant (
    assistant_id serial primary key,
    assistant_name varchar(50) NOT NULL 
);