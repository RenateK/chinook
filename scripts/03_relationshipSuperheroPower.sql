CREATE TABLE hero_power (
  hero_id INT NOT NULL,
  power_id INT NOT NULL,
  PRIMARY KEY (hero_id, power_id),
  FOREIGN KEY (hero_id)
      REFERENCES superhero (hero_id),
  FOREIGN KEY (power_id)
      REFERENCES power (power_id)
);
