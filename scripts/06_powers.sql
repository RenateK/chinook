INSERT INTO power (power_name, description) VALUES ('Hamstering', 'Heroes can hamster enemies and spit them out causing great damage');
INSERT INTO power (power_name, description) VALUES ('Invisibility', 'Heroes can be invisible from enemies');
INSERT INTO power (power_name, description) VALUES ('Sax Solo', 'Heroes can play a horrible saxophone solo');
INSERT INTO power (power_name, description) VALUES ('Gravity distortion', 'Heroes can alter gravity');

INSERT INTO hero_power (hero_id, power_id) VALUES(1, 1);
INSERT INTO hero_power (hero_id, power_id) VALUES(1, 2);
INSERT INTO hero_power (hero_id, power_id) VALUES(2, 3);
INSERT INTO hero_power (hero_id, power_id) VALUES(3, 2);
