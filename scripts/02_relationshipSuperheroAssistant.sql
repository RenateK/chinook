ALTER TABLE superhero ADD COLUMN assistant_id int REFERENCES assistant;

ALTER TABLE assistant ADD COLUMN hero_id int REFERENCES superhero;