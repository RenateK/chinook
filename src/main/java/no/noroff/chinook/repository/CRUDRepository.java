package no.noroff.chinook.repository;

import no.noroff.chinook.entity.Customer;
import no.noroff.chinook.entity.CustomerCountry;
import no.noroff.chinook.entity.CustomerGenre;
import no.noroff.chinook.entity.CustomerSpender;

import java.util.List;

public interface CRUDRepository<Customer, Integer> {
    List<Customer> findAll();
    Customer findById();
    Customer findByName();
    List<Customer> getCustomerByPage();
    int insert(Customer customer);
    int update(Customer customer);


}
