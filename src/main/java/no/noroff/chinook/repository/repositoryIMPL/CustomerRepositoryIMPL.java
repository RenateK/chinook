package no.noroff.chinook.repository.repositoryIMPL;

import no.noroff.chinook.entity.Customer;
import no.noroff.chinook.entity.CustomerCountry;
import no.noroff.chinook.entity.CustomerGenre;
import no.noroff.chinook.entity.CustomerSpender;
import no.noroff.chinook.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerRepositoryIMPL implements CustomerRepository {

    private final String url;
    private final String username;
    private final String password;

    public CustomerRepositoryIMPL(
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    /**
     *
     * This method is created to find all customer from customer table
     * method returns a list of customer with customer info as: customer id,
     * first name, last name, country, postal code, phone and email
     */
    @Override
    public List<Customer> findAll() {
        String sql = "SELECT * FROM customer";
        List<Customer> customers = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle result
            while(result.next()) {
                Customer customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
                customers.add(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }


    /**
     *
     * the method finds and returns a specific custumer given by customer id
     * sql query returns all information about the customer from customer table
     * In this case it does not take a customer as a parameter, id is given in the sql,
     * but it is possible to take a parameter and define id when calling the method
     */
    @Override
    public Customer findById() {
        Customer customer = null;
        String sql = "SELECT * FROM customer where customer_id=19";
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle result
            while(result.next()) {
                customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer ;
    }

    /**
     *
     * it is very similar to method findById() and finds a customer given by first name
     * Again it can take a customer as a parameter and define first name when the method is called
     */
    @Override
    public Customer findByName() {
        Customer customer = null;
        String sql = "SELECT * FROM customer where first_name LIKE 'Bjørn'";
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle result
            while(result.next()) {
                customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    /**
     *
     * method returns a list of customer from customer nr 10 and following 40 customers.
     */
    @Override
    public List<Customer> getCustomerByPage() {
        List<Customer> customers = new ArrayList<>();
        String sql = "SELECT * FROM customer order by customer_id limit 40 offset 10";
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle result
            while(result.next()) {
                Customer customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
                customers.add(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    /**
     *
     * @param customer
     * insert method is used to add a new customer to the database and returns a number of customers added to the database
     */
    @Override
    public int insert(Customer customer) {
        String sql = "INSERT INTO customer(customer_id, first_name, last_name, country, postal_code, phone, email ) VALUES (?,?,?,?,?,?,?)";
        int result = 0;

        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, customer.customer_id());
            statement.setString(2, customer.first_name());
            statement.setString(3, customer.last_name());
            statement.setString(4, customer.country());
            statement.setString(5, customer.postal_code());
            statement.setString(6, customer.phone());
            statement.setString(7, customer.email());

            // Execute statement
            result = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     *
     * @param customer
     * method updates users first name given by a specific user id. takes a customer as a parameter and returns
     * the number of customers updated
     */
    @Override
    public int update(Customer customer) {
        String sql = "UPDATE customer SET first_name='Gunne' WHERE customer_id=?";
        int result = 0;

        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, customer.customer_id());

            // Execute statement
            result = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     *
     * the method finds the country with most customers and returns the name of the country
     */
    @Override
    public CustomerCountry findMaxCountry() {
        String sql = "SELECT country, count(country) as test FROM customer GROUP BY (country) order by test DESC limit 1";
        CustomerCountry customerCountry = null;

        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            PreparedStatement statement = conn.prepareStatement(sql);

            // Execute statement
            ResultSet result = statement.executeQuery();

            while (result.next()){
                customerCountry = new CustomerCountry(
                        result.getString("country")
                );
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customerCountry;
    }

    /**
     *
     * finds the customer who has purchased the most and returns the customer id of this customer
     */
    @Override
    public CustomerSpender findHighestSpender() {
        String sql = "SELECT customer_id, total from invoice order by total desc limit 1";
        CustomerSpender customerSpender = null;

        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            PreparedStatement statement = conn.prepareStatement(sql);

            // Execute statement
            ResultSet result = statement.executeQuery();

            while (result.next()){
                customerSpender = new CustomerSpender(
                        result.getInt("customer_id")
                );
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customerSpender;
    }

    @Override
    public List<CustomerGenre> findHighestGenre() {
        return null;
    }
}
