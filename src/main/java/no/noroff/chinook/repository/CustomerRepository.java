package no.noroff.chinook.repository;

import no.noroff.chinook.entity.Customer;
import no.noroff.chinook.entity.CustomerCountry;
import no.noroff.chinook.entity.CustomerGenre;
import no.noroff.chinook.entity.CustomerSpender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.List;


public interface CustomerRepository extends CRUDRepository<Customer, Integer>{


    int insert(Customer customer);
    CustomerCountry findMaxCountry();
    CustomerSpender findHighestSpender();
    List<CustomerGenre> findHighestGenre();
}
